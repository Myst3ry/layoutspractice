package com.myst3ry.layoutspractice;

public final class IntentConstants {

    public static final String ACTION_SEND_TEXT = BuildConfig.APPLICATION_ID + "action.SEND_TEXT";
    public static final String ACTION_SEND_COLOR = BuildConfig.APPLICATION_ID + "action.SEND_COLOR";
    public static final String EXTRA_COLORS = BuildConfig.APPLICATION_ID + "extra.COLORS";
    public static final String EXTRA_TITLES = BuildConfig.APPLICATION_ID + "extra.TITLES";

    private IntentConstants() { }
}
