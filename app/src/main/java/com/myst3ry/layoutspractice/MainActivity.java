package com.myst3ry.layoutspractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.myst3ry.layoutspractice.fragment.SecondFragment;
import com.myst3ry.layoutspractice.fragment.ThirdFragment;

import java.util.Timer;
import java.util.TimerTask;

public final class MainActivity extends AppCompatActivity {

    private static final int EXCHANGE_DELAY = 100;
    private static final int EXCHANGE_PERIOD = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SecondFragment second = (SecondFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_second);
        final ThirdFragment third = (ThirdFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_third);
        startFragmentExchange(second, third);
    }

    private void startFragmentExchange(final SecondFragment secondFragment, final ThirdFragment thirdFragment) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                thirdFragment.updateColors(secondFragment.getTextColors());
                secondFragment.updateTitles(thirdFragment.getTextTitles());
            }
        }, EXCHANGE_DELAY, EXCHANGE_PERIOD);
    }
}
