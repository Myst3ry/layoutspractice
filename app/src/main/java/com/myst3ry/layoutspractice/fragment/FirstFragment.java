package com.myst3ry.layoutspractice.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.myst3ry.layoutspractice.IntentConstants;
import com.myst3ry.layoutspractice.R;
import com.myst3ry.layoutspractice.service.FirstService;

import java.util.List;

import butterknife.BindViews;

public final class FirstFragment extends BaseFragment {

    @BindViews({R.id.btn_one, R.id.btn_two, R.id.btn_three})
    List<Button> mButtons;

    private Context mContext;
    private ColorBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initReceiver();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    private void initReceiver() {
        mReceiver = new ColorBroadcastReceiver();
        mIntentFilter = new IntentFilter(IntentConstants.ACTION_SEND_COLOR);
    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.startService(FirstService.newIntent(mContext));
        mContext.registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(mReceiver);
    }


    private final class ColorBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final int[] colors = intent.getIntArrayExtra(IntentConstants.EXTRA_COLORS);
            if (colors != null) {
                setBackgroundColors(colors);
            }
        }

        private void setBackgroundColors(final int[] colors) {
            for (int i = 0; i < mButtons.size(); i++) {
                if (i < colors.length) {
                    mButtons.get(i).setBackgroundColor(colors[i]);
                }
            }
        }
    }
}
