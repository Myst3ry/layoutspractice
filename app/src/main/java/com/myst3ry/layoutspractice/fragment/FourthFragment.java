package com.myst3ry.layoutspractice.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.myst3ry.layoutspractice.R;
import com.myst3ry.layoutspractice.service.FourthBoundService;

import butterknife.BindView;

public final class FourthFragment extends BaseFragment {

    @BindView(R.id.btn_circular)
    Button mButtonCircular;
    @BindView(R.id.text_one)
    TextView mTextOne;
    @BindView(R.id.text_two)
    TextView mTextTwo;

    private ServiceConnection mConnection;
    private Messenger mMessenger;
    private Messenger mService;
    private Context mContext;
    private boolean isBound;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMessenger = new Messenger(new FourthFragmentHandler());
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = new Messenger(service);
                final Message message = Message.obtain(null, FourthBoundService.MSG_REGISTER_CLIENT);
                message.replyTo = mMessenger;

                try {
                    mService.send(message);
                } catch (RemoteException re) {
                    re.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }
        };
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_fourth, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        bindService();
    }

    @Override
    public void onPause() {
        super.onPause();
        unbindService();
    }

    private void bindService() {
        mContext.bindService(FourthBoundService.newIntent(mContext), mConnection, Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    private void unbindService() {
        if (isBound) {
            final Message message = Message.obtain(null, FourthBoundService.MSG_UNREGISTER_CLIENT);
            message.replyTo = mMessenger;
            try {
                mService.send(message);
            } catch (RemoteException re) {
                re.printStackTrace();
            }

            mContext.unbindService(mConnection);
            isBound = false;
        }
    }

    private final class FourthFragmentHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            final ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mButtonCircular.getLayoutParams();
            final int currentAngle = (int) msg.obj;
            params.circleAngle = currentAngle;
            mButtonCircular.setText(String.format(getString(R.string.btn_text_angle), currentAngle));
            mButtonCircular.setLayoutParams(params);
            mTextOne.setText(String.format(getString(R.string.text_one), FourthBoundService.ANGLE_MAX - currentAngle));
            mTextTwo.setText(String.format(getString(R.string.text_two), FourthBoundService.ANGLE_STEP));
        }
    }

}
