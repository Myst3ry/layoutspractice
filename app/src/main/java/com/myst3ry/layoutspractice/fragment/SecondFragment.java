package com.myst3ry.layoutspractice.fragment;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.myst3ry.layoutspractice.R;
import com.myst3ry.layoutspractice.service.SecondBoundService;

import java.util.List;
import java.util.Random;

import butterknife.BindViews;

public final class SecondFragment extends BaseFragment {

    @BindViews({R.id.btn_one, R.id.btn_two, R.id.btn_three, R.id.btn_four, R.id.btn_five,
            R.id.btn_six, R.id.btn_seven, R.id.btn_eight, R.id.btn_nine})
    List<Button> mButtons;

    private Context mContext;
    private Messenger mService;
    private Messenger mMessenger;
    private ServiceConnection mConnection;
    private boolean isBound;
    private Handler mHandler;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        mMessenger = new Messenger(new SecondFragmentHandler());
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = new Messenger(service);
                final Message message = Message.obtain(null, SecondBoundService.MSG_REGISTER_CLIENT);
                message.replyTo = mMessenger;

                try {
                    mService.send(message);
                } catch (RemoteException re) {
                    re.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
            }
        };
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        bindService();
    }

    @Override
    public void onPause() {
        super.onPause();
        unbindService();
    }

    public int[] getTextColors() {
        final int[] textColors = new int[mButtons.size()];
        for (int i = 0; i < textColors.length; i++) {
            textColors[i] = mButtons.get(i).getCurrentTextColor();
        }

        return textColors;
    }

    public void updateTitles(final String[] titles) {

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                for (final String title : titles) {
                    final int pos = new Random().nextInt(mButtons.size());
                    mButtons.get(pos).setText(title);
                }
            }
        });
    }

    private void bindService() {
        mContext.bindService(SecondBoundService.newIntent(mContext), mConnection, Context.BIND_AUTO_CREATE);
        isBound = true;
    }

    private void unbindService() {
        if (isBound) {
            final Message message = Message.obtain(null, SecondBoundService.MSG_UNREGISTER_CLIENT);
            message.replyTo = mMessenger;
            try {
                mService.send(message);
            } catch (RemoteException re) {
                re.printStackTrace();
            }

            mContext.unbindService(mConnection);
            isBound = false;
        }
    }

    private final class SecondFragmentHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            final int[] colors = (int[]) msg.obj;
            if (colors != null) {
                setTextColors(colors);
            }
        }

        private void setTextColors(final int[] colors) {
            for (int i = 0; i < mButtons.size(); i++) {
                if (i < colors.length) {
                    mButtons.get(i).setTextColor(colors[i]);
                }
            }
        }
    }

}
