package com.myst3ry.layoutspractice.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.myst3ry.layoutspractice.IntentConstants;
import com.myst3ry.layoutspractice.R;
import com.myst3ry.layoutspractice.service.ThirdService;

import java.util.List;
import java.util.Random;

import butterknife.BindViews;

public final class ThirdFragment extends BaseFragment {

    @BindViews({R.id.btn_one, R.id.btn_two, R.id.btn_three})
    List<Button> mButtons;

    private Context mContext;
    private TextBroadcastReceiver mReceiver;
    private IntentFilter mIntentFilter;
    private Handler mHandler;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
        initReceiver();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_third, container, false);
    }

    private void initReceiver() {
        mReceiver = new TextBroadcastReceiver();
        mIntentFilter = new IntentFilter(IntentConstants.ACTION_SEND_TEXT);
    }

    @Override
    public void onResume() {
        super.onResume();
        mContext.startService(ThirdService.newIntent(mContext));
        mContext.registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();
        mContext.unregisterReceiver(mReceiver);
    }

    public String[] getTextTitles() {
        final String[] textTitles = new String[mButtons.size()];
        for (int i = 0; i < textTitles.length; i++) {
            textTitles[i] = mButtons.get(i).getText().toString();
        }

        return textTitles;
    }

    public void updateColors(final int[] colors) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                for (final Button btn : mButtons) {
                    final int pos = new Random().nextInt(colors.length);
                    btn.setTextColor(colors[pos]);
                }
            }
        });
    }


    private final class TextBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String[] titles = intent.getStringArrayExtra(IntentConstants.EXTRA_TITLES);
            if (titles != null) {
                setTitles(titles);
            }
        }

        private void setTitles(final String[] titles) {
            for (int i = 0; i < mButtons.size(); i++) {
                if (i < titles.length) {
                    mButtons.get(i).setText(titles[i]);
                }
            }
        }
    }
}