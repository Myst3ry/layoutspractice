package com.myst3ry.layoutspractice.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.myst3ry.layoutspractice.IntentConstants;
import com.myst3ry.layoutspractice.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public final class FirstService extends Service {

    private static final int ITEMS_COUNT = 3;
    private static final int FIRST_SERVICE_DELAY = 100;
    private static final int FIRST_SERVICE_PERIOD = 500;

    @Override
    public void onCreate() {
        super.onCreate();
        final int[] colors = getApplicationContext().getResources().getIntArray(R.array.random_colors_array);
        startBroadcast(colors);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, FirstService.class);
    }

    private void startBroadcast(final int[] colors) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final int[] randomColors = new int[ITEMS_COUNT];
                for (int i = 0; i < ITEMS_COUNT; i++) {
                    final int pos = new Random().nextInt(colors.length);
                    randomColors[i] = colors[pos];
                }

                final Intent sendIntent = new Intent();
                sendIntent.setAction(IntentConstants.ACTION_SEND_COLOR);
                sendIntent.putExtra(IntentConstants.EXTRA_COLORS, randomColors);
                sendBroadcast(sendIntent);
            }
        }, FIRST_SERVICE_DELAY, FIRST_SERVICE_PERIOD);
    }
}
