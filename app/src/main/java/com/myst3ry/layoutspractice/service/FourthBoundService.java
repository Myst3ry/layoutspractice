package com.myst3ry.layoutspractice.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public final class FourthBoundService extends Service {

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int ANGLE_MAX = 360;
    public static final int ANGLE_STEP = 4;

    private static final int FOURTH_SERVICE_DELAY = 0;
    private static final int FOURTH_SERVICE_PERIOD = 100;

    private List<Messenger> mClients;
    private Messenger mMessenger;
    private int mCurrentAngle = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mClients = new ArrayList<>();
        mMessenger = new Messenger(new FourthServiceHandler());
        startMessaging();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, FourthBoundService.class);
    }

    private void startMessaging() {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for (final Messenger client : mClients) {
                    final Message message = new Message();
                    if (mCurrentAngle < ANGLE_MAX) {
                        mCurrentAngle += ANGLE_STEP;
                    } else {
                        mCurrentAngle = ANGLE_STEP;
                    }

                    message.obj = mCurrentAngle;

                    try {
                        client.send(message);
                    } catch (RemoteException re) {
                        re.printStackTrace();
                        stopSelf();
                    }
                }
            }
        }, FOURTH_SERVICE_DELAY, FOURTH_SERVICE_PERIOD);
    }

    private final class FourthServiceHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    if (mClients.isEmpty()) {
                        stopSelf();
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
