package com.myst3ry.layoutspractice.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.myst3ry.layoutspractice.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public final class SecondBoundService extends Service {

    public static final int MSG_REGISTER_CLIENT = 101;
    public static final int MSG_UNREGISTER_CLIENT = 102;

    private static final int ITEMS_COUNT = 9;
    private static final int SECOND_SERVICE_DELAY = 100;
    private static final int SECOND_SERVICE_PERIOD = 250;

    private List<Messenger> mClients;
    private Messenger mMessenger;

    @Override
    public void onCreate() {
        super.onCreate();
        mClients = new ArrayList<>();
        mMessenger = new Messenger(new SecondServiceHandler());
        final int[] colors = getApplicationContext().getResources().getIntArray(R.array.random_colors_array);
        startMessaging(colors);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, SecondBoundService.class);
    }

    private void startMessaging(final int[] colors) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final int[] randomColors = new int[ITEMS_COUNT];
                for (final Messenger client : mClients) {
                    final Message message = new Message();
                    for (int i = 0; i < ITEMS_COUNT; i++) {
                        final int pos = new Random().nextInt(colors.length);
                        randomColors[i] = colors[pos];
                    }

                    message.obj = randomColors;

                    try {
                        client.send(message);
                    } catch (RemoteException re) {
                        re.printStackTrace();
                        stopSelf();
                    }
                }
            }
        }, SECOND_SERVICE_DELAY, SECOND_SERVICE_PERIOD);
    }

    private final class SecondServiceHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    if (mClients.isEmpty()) {
                        stopSelf();
                    }
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
}
