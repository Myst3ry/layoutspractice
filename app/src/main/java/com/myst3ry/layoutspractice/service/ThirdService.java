package com.myst3ry.layoutspractice.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import com.myst3ry.layoutspractice.IntentConstants;
import com.myst3ry.layoutspractice.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public final class ThirdService extends Service {

    private static final int ITEMS_COUNT = 3;
    private static final int THIRD_SERVICE_DELAY = 100;
    private static final int THIRD_SERVICE_PERIOD = 1000;

    @Override
    public void onCreate() {
        super.onCreate();
        final String[] titles = getApplicationContext().getResources().getStringArray(R.array.random_string_array);
        startBroadcast(titles);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, ThirdService.class);
    }

    private void startBroadcast(final String[] titles) {
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                final String[] randomTitles = new String[ITEMS_COUNT];
                for (int i = 0; i < ITEMS_COUNT; i++) {
                    final int pos = new Random().nextInt(titles.length);
                    randomTitles[i] = titles[pos];
                }

                final Intent sendIntent = new Intent();
                sendIntent.setAction(IntentConstants.ACTION_SEND_TEXT);
                sendIntent.putExtra(IntentConstants.EXTRA_TITLES, randomTitles);
                sendBroadcast(sendIntent);
            }
        }, THIRD_SERVICE_DELAY, THIRD_SERVICE_PERIOD);
    }
}
